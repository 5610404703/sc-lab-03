package view;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import model.Search;

public class SearchGUI {
	
	private JFrame frame;
	private JPanel panelUp, panelDown, upLeft, upRight, centerLeft, downLeft, downRight;
	private TextArea boxInput;
	private TextArea boxResult;
	private TextField boxNgram;
	private JButton submitButton,test1Button,test2Button;
	private JRadioButton ngramButton, countButton;
	protected int count;
	private String test1= "Hello my name is Oranuch Charoennan.\nShe is my friend.Her name is Pimnipa Pituktigul";
	private String test2= "Hello my name is Oranuch Charoennan.\t\t\tShe is my friend.Her name is Pimnipa Pituktigul";

	
	public SearchGUI() {
		createPanel();
		createPanel();
		createTextAreaInput();
		createTextAreaResult();
		creatButton();
		createRadioButton();
		createTextField();
		showMassage();
		createFrame(); 
   }
	
	public void createPanel(){
		panelUp = new JPanel();
		panelUp.setLayout(new BorderLayout());
		panelDown = new JPanel();
		panelDown.setLayout(new BorderLayout());
		upLeft = new JPanel();
		upLeft.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Input Sentence:", TitledBorder.LEADING, TitledBorder.TOP));
		upRight = new JPanel();
		upRight.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Result:", TitledBorder.LEADING, TitledBorder.TOP));
		centerLeft = new JPanel();
		downLeft = new JPanel();
		downRight = new JPanel();
	}
	
	
	public void createTextAreaInput(){
		boxInput = new TextArea();
	}
	
	
	public void createTextAreaResult(){
		boxResult = new TextArea();
	}
	
	
	public void creatButton(){
		submitButton = new JButton("Submit");
		test1Button = new JButton("Test1");
		test2Button = new JButton("Test2");
	}
	
	
	public void createRadioButton() {
		countButton = new JRadioButton("word");
		countButton.setMnemonic(KeyEvent.VK_B);
		countButton.setActionCommand("word");
		countButton.setSelected(true);
		
		ngramButton = new JRadioButton("n-gram");
		ngramButton.setMnemonic(KeyEvent.VK_B);
		ngramButton.setActionCommand("n-gram");
		ngramButton.setSelected(true);
	}
	
	
	public void createTextField(){
		boxNgram = new TextField("3");
	}
	
	
	public void showMassage(){
	
		boxResult.setText("if selected n-gram,Plase input gram.");

	}
	
	
   public void setListener(ActionListener list) {
		submitButton.addActionListener(list);
   }
   
   
   public void createFrame() {
		frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.add(panelUp, BorderLayout.NORTH);
		frame.add(panelDown, BorderLayout.CENTER);
		
		panelUp.add(upLeft, BorderLayout.WEST);
		panelUp.add(upRight,BorderLayout.EAST);
		
		panelDown.add(centerLeft, BorderLayout.CENTER);
		panelDown.add(downLeft,BorderLayout.WEST); 
		panelDown.add(downRight,BorderLayout.EAST); 
		
		upLeft.add(boxInput, BorderLayout.CENTER);
		upRight.add(boxResult, BorderLayout.CENTER);
		
		downLeft.add(test1Button);
		downLeft.add(test2Button);
		centerLeft.add(submitButton);
		
		downRight.add(countButton, BorderLayout.WEST);
		downRight.add(ngramButton, BorderLayout.WEST);
		downRight.add(boxNgram, BorderLayout.WEST);
		
		boxResult.setEditable(false);
		
		test1Button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				boxInput.setText(test1);
				
			}	
		});
		
		
		test2Button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				boxInput.setText(test2);
			}
		});
		
		
		submitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				Search search = new Search();
				if (countButton.isSelected() && ngramButton.isSelected()){
					count = search.Count(boxInput.getText());
					int n = Integer.parseInt(boxNgram.getText());
					String token = search.notSpace(boxInput.getText());
					StringBuffer ngram = search.subStr(token, n);
					boxResult.setText(count +" word\n"+ngram);
				}
				
				else if (ngramButton.isSelected()){
					int n = Integer.parseInt(boxNgram.getText());
					String token = search.notSpace(boxInput.getText());
					StringBuffer ngram = search.subStr(token, n);
					boxResult.setText(ngram.toString());
				}
				
				else if (countButton.isSelected()){
					count = search.Count(boxInput.getText());
					boxResult.setText(count +" word\n");
				}
				
			}		
		});
		
		 
		frame.pack();
		frame.setLayout(null);
		frame.setSize(927,300);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
   }


   public TextArea getInputField() {
	   return boxInput;
   }
 


 
}
