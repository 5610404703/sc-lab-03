package model;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Search {

	private String token = "";

	public String notSpace(String input) {
		StringTokenizer str = new StringTokenizer(input);
		while (str.hasMoreElements()) {
			
			token += str.nextElement().toString();
		}
		return token;
	}
	
	int count = 0;
	public StringBuffer subStr(String token, int n) {
		
		StringBuffer list = new StringBuffer();

		for (int i = 0; i < token.length() - n + 1; i++, count++) {
			list.append(token.substring(i, n + i));

			if (i < token.length() - n) {
				list.append(",");
			}
		}
		return list;
	}

	
	public int Count(String input){
		ArrayList<String> l = new ArrayList<String>();
		StringTokenizer str = new StringTokenizer(input);

		while (str.hasMoreElements()) {
			l.add(str.nextElement().toString());
		}
		return l.size();
	}

}
